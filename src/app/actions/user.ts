import { Action } from '@ngrx/store';
import { type } from '../util';

export const ActionTypes = {
  LOGIN:         type('[User] Login'),
  LOGIN_SUCCED:  type('[User] Login Complete'),
  LOGIN_FAILED:  type('[User] Login Failed'),
  LOGOUT:        type('[User] Logout'),
};

interface LoginCred {
    user: string;
    password: string;
}
export class LoginAction implements Action {
  type = ActionTypes.LOGIN;

  constructor(public payload: LoginCred) { }
}

export class LoginSuccedAction implements Action {
  type = ActionTypes.LOGIN_SUCCED;

  constructor(public payload: {}) { }
}

export class LoginFailedAction implements Action {
  type = ActionTypes.LOGIN_FAILED;

  constructor(public payload: {}) { }
}

export class LogoutAction implements Action {
  type = ActionTypes.LOGOUT;

  constructor() { }
}

export type Actions
  = LoginAction
  | LoginSuccedAction
  | LoginFailedAction
  | LogoutAction
