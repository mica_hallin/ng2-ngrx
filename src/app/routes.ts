import { Routes } from '@angular/router';

import { MainPageComponent } from './containers/main';
import { SecondPageComponent } from './containers/second';

export const routes: Routes = [
  {
    path: '',
    component: MainPageComponent
  },
  {
    path: 'secondPage',
    component: SecondPageComponent
  }
];
