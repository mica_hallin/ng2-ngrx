
import * as fromRouter from '@ngrx/router-store';
import * as fromAuth from './auth';
import { ActionReducer } from '@ngrx/store';
import { compose } from '@ngrx/core/compose';
import { storeFreeze } from 'ngrx-store-freeze';
import { combineReducers } from '@ngrx/store';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

export interface State {
  router: fromRouter.RouterState;
  auth: fromAuth.State
}

const reducers = {
  router: fromRouter.routerReducer,
  auth: fromAuth.reducer
};

const developmentReducer: ActionReducer<State> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<State> = combineReducers(reducers);


export function reducer(state: any, action: any) {
  if (environment.production) {
    return productionReducer(state, action);
  }
  else {
    return developmentReducer(state, action);
  }
}


export function getAuthState(state$: Observable<State>) {
  return state$.select(s => s.auth);
}

export const isLogged = compose(fromAuth.isLogged, getAuthState);