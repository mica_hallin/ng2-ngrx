import '@ngrx/core/add/operator/select';
import { Observable } from 'rxjs/Observable';
import { ActionTypes, Actions as UserAction } from '../actions/user';


export interface State {
  loggedIn: boolean;
  loading: boolean;
  user: any;
};

const initialState: State = {
  loggedIn: false,
  loading: false,
  user: {}
};

export function reducer(state = initialState, action: UserAction): State {
  switch (action.type) {
    case ActionTypes.LOGIN_SUCCED:
        console.log('Login SUcced');
        return {
          loggedIn: true,
          loading: false,
          user: { name : 'SuperUser'}
        };

    case ActionTypes.LOGIN:
      return {
        loggedIn: false,
        loading: true,
        user : {}
      };

    case ActionTypes.LOGOUT:
      return initialState;

    default: {
      return state;
    }
  }
}

export function getUser(state$: Observable<State>) {
  return state$.select(state => state.user);
}

export function isLogged(state$: Observable<State>) {
  return state$.select(state => state.loggedIn);
}

export function getLoading(state$: Observable<State>) {
  return state$.select(state => state.loading);
}