import {Injectable} from "@angular/core";
import {CanActivate, Router} from "@angular/router";
import {UserService} from "./user.service";


@Injectable()
export class AuthGuard implements CanActivate {

    auth: any = {};

    constructor(private authService: UserService, private router: Router) {

    }

    canActivate() {
        return this.authService.isLoggedIn().map(isSuccess => {
                if (isSuccess == true) return isSuccess;
                else {
                    this.router.navigate(['login']);
                    return false;
                }
            }
        );
    }
}