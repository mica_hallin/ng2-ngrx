import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { StoreModule } from '@ngrx/store';
import { RouterStoreModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppComponent } from './app.component';
import { EffectsModule } from '@ngrx/effects';

import { AuthEffects } from './effects/auth';
import { reducer } from './reducers';
import { routes } from './routes';
import { UsersService } from './services/users';
import { MainPageComponent } from './containers/main';
import { SecondPageComponent } from './containers/second';


@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    SecondPageComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpModule,
    StoreModule.provideStore(reducer),
    RouterModule.forRoot(routes, { useHash: true }),
    RouterStoreModule.connectRouter(),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
    EffectsModule.run(AuthEffects),
  ],
  providers: [ UsersService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
