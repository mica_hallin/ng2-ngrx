import { Component, ChangeDetectionStrategy } from '@angular/core';


@Component({
  selector: 'bc-second-bage',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h1>Second Page</h1>
    <button routerLink="/">Go back</button> 
  `,
  styles: [`
    :host {
      text-align: center;
    }
  `]
})
export class SecondPageComponent { }
