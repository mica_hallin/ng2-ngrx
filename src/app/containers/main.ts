import { Component, ChangeDetectionStrategy } from '@angular/core';
import { go, replace, search, show, back, forward } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { State } from '../reducers';
import * as fromRoot from '../reducers';
import * as user from '../actions/user';
@Component({
  selector: 'bc-main-bage',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h1>Main Page</h1>
    <button routerLink="/secondPage">Go next Page</button>
    <button *ngIf="!(isLogged | async)" (click)="login()">Login</button>
    <button *ngIf="(isLogged | async)" (click)="logout()">Logout</button> {{isLogged | async}}
  `,
  styles: [`
    :host {
      text-align: center;
    }
  `]
})
export class MainPageComponent {
    isLogged;
    constructor(private store:Store<fromRoot.State>) {
       this.isLogged = store.let(fromRoot.isLogged);
    }

    login() {
        this.store.dispatch(new user.LoginAction( {user: 'test', password: 'test'}));
    }
    logout() {
         this.store.dispatch(new user.LogoutAction());
    }
}
