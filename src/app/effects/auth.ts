import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Actions, Effect } from '@ngrx/effects';
import { UsersService } from '../services/users';
import { Observable } from 'rxjs/Observable';
import { ActionTypes }from '../actions/user';

@Injectable()
export class AuthEffects {

  @Effect() login$ = this.actions$
      // Listen for the 'LOGIN' action
      .ofType(ActionTypes.LOGIN)
      // Map the payload into JSON to use as the request body
      .map(action => action.payload)
      .switchMap(payload => this.usersService.login(payload.user,payload.password)
        // If successful, dispatch success action with result
        .map(res => ({ type:  ActionTypes.LOGIN_SUCCED, payload: res }))
        // If request fails, dispatch failed action
        .catch(() => Observable.of({ type: ActionTypes.LOGIN_FAILED }))
      );

  constructor(
    private http: Http,
    private actions$: Actions,
    private usersService: UsersService
  ) { }
}
